# php-extended/php-datetime-parser-object

A library that implements php-extended/php-datetime-parser-interface library.

![coverage](https://gitlab.com/php-extended/php-datetime-parser-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-datetime-parser-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-datetime-parser-object ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\DateTime\DateTimeImmutableParser();

$dtp = new DateTimeImmutableParser();
$dtp->addFormat('<my custom format>'); // @see DateTime:createFromFormat()

try
{
	$dt = $dtp->parse('<my to-be-parsed date-string>');
	// $dt instanceof \DateTimeImmutable
	// do stgh
}
catch(ParseException $e)
{
	// do stgh else
}

```


## License

MIT (See [license file](LICENSE)).
