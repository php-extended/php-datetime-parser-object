<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-datetime-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DateTime;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use PhpExtended\Parser\ParseException;

/**
 * DateTimeParser class file.
 * 
 * This class parses datetime strings into native \DateTime objects.
 * 
 * @author Anastaszor
 */
class DateTimeParser extends AbstractDateTimeParser
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 * @psalm-suppress MixedInferredReturnType
	 */
	public function parse(?string $data) : DateTimeInterface
	{
		$data = (string) $data;
		
		// cant call DateTime:: because formatter transforms it to DateTimeImmutable
		$class = 'DateTime';
		
		$datetime = $this->parseAsInteger($data);
		if(null !== $datetime)
		{
			return $datetime;
		}
		
		$datetime = $this->parseAsFloat($data);
		if(null !== $datetime)
		{
			return $datetime;
		}
		
		foreach($this->_formats as $format)
		{
			$datetime = $class::createFromFormat($format, $data);
			if(false !== $datetime)
			{
				/** @psalm-suppress MixedReturnStatement */
				return $datetime;
			}
		}
		
		$message = 'Failed to parse DateTime value "{value}", format tried are : "{list}"';
		$context = ['{value}' => $data, '{list}' => \implode('", "', $this->_formats)];
		
		throw new ParseException(DateTimeImmutable::class, $data, 0, \strtr($message, $context));
	}
	
	/**
	 * Parses a datetime if the data represents integer data.
	 *
	 * @param string $data
	 * @return ?DateTimeInterface
	 * @psalm-suppress MixedInferredReturnType
	 */
	protected function parseAsInteger(string $data) : ?DateTimeInterface
	{
		if(\preg_match('#^\\d+$#', $data))
		{
			if(12 > \mb_strlen($data)) // seconds
			{
				try
				{
					return new DateTimeImmutable('@'.$data);
				}
				catch(Exception $e)
				{
					return null;
				}
			}
			
			if(15 > \mb_strlen($data)) // milliseconds
			{
				$data .= '000';
			}
			
			// microseconds
			$data = ((string) \mb_substr($data, 0, -6)).'.'.((string) \mb_substr($data, -6));
			
			$class = 'DateTime';
			$datetime = $class::createFromFormat('U.u', $data);
			if(false !== $datetime)
			{
				/** @psalm-suppress MixedReturnStatement */
				return $datetime;
			}
		}
		
		return null;
	}
	
	/**
	 * Parses a datetime if the data represents float data.
	 *
	 * @param string $data
	 * @return ?DateTimeInterface
	 * @psalm-suppress MixedInferredReturnType
	 */
	protected function parseAsFloat(string $data) : ?DateTimeInterface
	{
		if(\preg_match('#^\\d+\\.\\d+$#', $data))
		{
			$intpart = (string) ((int) $data);
			$dotpos = (int) \mb_strpos($data, '.');
			$intlen = (int) \mb_strlen($intpart);
			
			$realdata = ((string) \mb_substr($data, 0, $dotpos + 7));
			
			if(18 > $intlen && 15 <= $intlen) // microseconds
			{
				$realdata = ((string) \mb_substr($intpart, 0, $intlen - 6)).'.'.((string) \mb_substr($intpart, -6));
			}
			
			if(15 > $intlen && 12 <= $intlen) // milliseconds
			{
				$realdata = ((string) \mb_substr($intpart, 0, $intlen - 3)).'.'.((string) \mb_substr($intpart, -3)).((string) \mb_substr($data, $dotpos + 1, 3));
			}
			
			$class = 'DateTime';
			$datetime = $class::createFromFormat('U.u', $realdata);
			if(false !== $datetime)
			{
				/** @psalm-suppress MixedReturnStatement */
				return $datetime;
			}
		}
		
		return null;
	}
	
}
