<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-datetime-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DateTime;

use DateTime;
use PhpExtended\Parser\AbstractParser;

/**
 * AbstractDateTimeParser class file.
 * 
 * This class manages the parsing formats for concrete datetime parsers.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\AbstractParser<\DateTimeInterface>
 */
abstract class AbstractDateTimeParser extends AbstractParser implements DateTimeParserInterface
{
	
	/**
	 * The formats to use to parse.
	 * 
	 * @var array<integer, string>
	 */
	protected $_formats = [
		'!Y-m-d H:i:s',
		'!Y-m-d',
		'!H:i:s',
		'!H:i',
		'!d/m/Y H:i:s',
		'!d/m/Y H:i',
		'!d/m/Y',
		'!'.DateTime::W3C,
		'!'.DateTime::RFC3339_EXTENDED,
		'!'.DateTime::RFC3339,
		'!'.DateTime::ATOM,
		'!'.DateTime::COOKIE,
		'!'.DateTime::ISO8601,
		'!'.DateTime::RFC822,
		'!'.DateTime::RFC1036,
		'!'.DateTime::RFC1123,
		'!'.DateTime::RFC2822,
		'!'.DateTime::RSS,
	];
	
	/**
	 * Builds a new AbstractDateTimeParser with the given additional formats.
	 * 
	 * @param array<integer, string> $additionalFormats
	 */
	public function __construct(array $additionalFormats = [])
	{
		$this->_formats = \array_merge(\array_values($additionalFormats), $this->_formats);
	}
	
	/**
	 * Gets the current known formats for this datetime parser.
	 * 
	 * @return array<integer, string>
	 */
	public function getFormats() : array
	{
		return $this->_formats;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTime\DateTimeParserInterface::setFormats()
	 */
	public function setFormats(array $formats) : DateTimeParserInterface
	{
		if(!empty($formats))
		{
			$this->_formats = \array_values($formats);
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\DateTime\DateTimeParserInterface::addFormat()
	 */
	public function addFormat(string $format) : DateTimeParserInterface
	{
		if(!empty($format))
		{
			\array_unshift($this->_formats, $format);
		}
		
		return $this;
	}
	
}
