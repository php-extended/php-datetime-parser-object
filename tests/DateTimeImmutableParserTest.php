<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-datetime-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DateTime\DateTimeImmutableParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * DateTimeImmutableParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DateTime\DateTimeImmutableParser
 *
 * @internal
 *
 * @small
 */
class DateTimeImmutableParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var DateTimeImmutableParser
	 */
	protected DateTimeImmutableParser $_parser;
	
	public function testCantBeParsed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('toto');
	}
	
	public function testCanBeParsed() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_parser->parse('2000-01-01'));
	}
	
	public function testTimestamp() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2020-10-06 05:27:28'), $this->_parser->parse('1601962048'));
	}
	
	public function testMillitimestamp() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2020-10-06 05:27:28'), $this->_parser->parse('1601962048000'));
	}
	
	public function testMicrotimestamp() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2020-10-06 05:27:28'), $this->_parser->parse('1601962048000000'));
	}
	
	public function testTimestampFloat() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s.u', '2020-10-06 05:27:28.234567'), $this->_parser->parse('1601962048.2345678'));
	}
	
	public function testMillitimestampFloat() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s.u', '2020-10-06 05:27:28.234567'), $this->_parser->parse('1601962048234.567890'));
	}
	
	public function testMicrotimestampFloat() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s.u', '2020-10-06 05:27:28.234567'), $this->_parser->parse('1601962048234567.110'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		\date_default_timezone_set('GMT');
		$this->_parser = new DateTimeImmutableParser();
	}
	
}
