<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-datetime-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DateTime\AbstractDateTimeParser;
use PHPUnit\Framework\TestCase;

/**
 * AbstractDateTimeParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DateTime\AbstractDateTimeParser
 *
 * @internal
 *
 * @small
 */
class AbstractDateTimeParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var AbstractDateTimeParser
	 */
	protected AbstractDateTimeParser $_adtp;
	
	public function testSetFormats() : void
	{
		$this->assertEquals($this->_adtp, $this->_adtp->setFormats([]));
	}
	
	public function testSetFormats2() : void
	{
		$this->assertEquals(['format'], $this->_adtp->setFormats(['format'])->getFormats());
	}
	
	public function testAddFormats() : void
	{
		$this->assertEquals($this->getMockForAbstractClass(AbstractDateTimeParser::class, [['format']]), $this->_adtp->addFormat('format'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_adtp = $this->getMockForAbstractClass(AbstractDateTimeParser::class);
	}
	
}
