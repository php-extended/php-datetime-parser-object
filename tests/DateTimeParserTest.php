<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-datetime-parser-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\DateTime\DateTimeParser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * DateTimeParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\DateTime\DateTimeParser
 *
 * @internal
 *
 * @small
 */
class DateTimeParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var DateTimeParser
	 */
	protected DateTimeParser $_parser;
	
	public function testCantBeParsed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse('toto');
	}
	
	public function testCanBeParsed() : void
	{
		// cant call DateTime:: because formatter transforms it to DateTimeImmutable
		$class = 'DateTime';
		$this->assertEquals($class::createFromFormat('!Y-m-d', '2000-01-01'), $this->_parser->parse('2000-01-01'));
	}
	
	public function testTimestamp() : void
	{
		$class = 'DateTime';
		$this->assertEquals($class::createFromFormat('!Y-m-d H:i:s', '2020-10-06 05:27:28'), $this->_parser->parse('1601962048'));
	}
	
	public function testMillitimestamp() : void
	{
		$class = 'DateTime';
		$this->assertEquals($class::createFromFormat('!Y-m-d H:i:s', '2020-10-06 05:27:28'), $this->_parser->parse('1601962048000'));
	}
	
	public function testMicrotimestamp() : void
	{
		$class = 'DateTime';
		$this->assertEquals($class::createFromFormat('!Y-m-d H:i:s', '2020-10-06 05:27:28'), $this->_parser->parse('1601962048000000'));
	}
	
	public function testTimestampFloat() : void
	{
		$class = 'DateTime';
		$this->assertEquals($class::createFromFormat('!Y-m-d H:i:s.u', '2020-10-06 05:27:28.234567'), $this->_parser->parse('1601962048.2345678'));
	}
	
	public function testMillitimestampFloat() : void
	{
		$class = 'DateTime';
		$this->assertEquals($class::createFromFormat('!Y-m-d H:i:s.u', '2020-10-06 05:27:28.234567'), $this->_parser->parse('1601962048234.567890'));
	}
	
	public function testMicrotimestampFloat() : void
	{
		$class = 'DateTime';
		$this->assertEquals($class::createFromFormat('!Y-m-d H:i:s.u', '2020-10-06 05:27:28.234567'), $this->_parser->parse('1601962048234567.110'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		\date_default_timezone_set('GMT');
		$this->_parser = new DateTimeParser();
	}
	
}
